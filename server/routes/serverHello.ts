import express, { Request, Response } from "express";
const router = express.Router()

router.get('/serverHello', (_ : Request, res : Response) => {
  console.log('hello from server')
  res.status(200).json({ text: 'Hello From Server' })
})


export default router