import express, {Request, Response, NextFunction} from "express";
import serverHello from './routes/serverHello'
const port = process.env.PORT || 3001;

import { resolve } from "path"
import { config } from "dotenv"
config({ path: resolve(__dirname, "../.env") });
// Create a new express app instance


(async () => {
  try {
    const server = express();

    server.use(function(_ : Request, res :Response, next : NextFunction) {
      res.header('Access-Control-Allow-Origin', '*');
      next();
    })
    server.use('/api', serverHello)

    server.listen(port, (err?: any) => {
      if (err) throw err;
      console.log(`> Ready on localhost:${port} - env ${process.env.NODE_ENV}`);
    });
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
})();